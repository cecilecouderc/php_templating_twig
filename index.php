<?php
require_once 'vendor/autoload.php';

//initialisation FAker
$faker = Faker\Factory::create();


// rooting
$page = 'home';
if(isset($_GET['p'])){
    $page = $_GET['p'];
}

//rendu du template 
$loader = new Twig_Loader_Filesystem(__DIR__. '/template');
$twig = new Twig_Environment($loader, [
    // 'cache' => __DIR__ . '/temp'
    'cache' => false,
]);

if($_GET['p'] === 'home'){
    echo $twig->render('content.twig', [
        'Compagny_Name'=> $faker->name, 
        'productAdjective'=> $faker->text,
        'productname' => $faker->text,
        'productMateriel' => $faker->text,
        'Url' => 'http://url.com',
        'Price' => $faker->randomNumber(2),
        'UserName' => $faker->name,
        'Color' => $faker->safeColorName,
        'Job' => $faker->jobTitle,
        'imgProduct' => $faker-> imageUrl($width = 300, $height = 200),
        'picture' => $faker -> imageUrl($width = 250, $height = 250, 'people')
        ]);
}
?>
